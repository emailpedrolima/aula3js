/*Criar uma função que, dado um número indefinido de arrays, faça a soma de seus valores Ex: [1,2,3] [1,2,2] [1,1] => 13 [1,1] [2, 20] => 24*/

function somaArrays(...arrays){
   let soma = 0;
   
   for (i in arrays){
      currArr = arrays[i]
      for (j in currArr){
         soma +=currArr[j]
      }
   }
   console.log(soma)
};

somaArrays([1,2,3],[3,3,3],[1]);



/*Criar uma função que dado um número n e um array, retorne um novo array com os valores do array anterior * n Ex: (2, [1,3,6,10]) => [2,6,12,20] (3, [7,9,11,-2]) => [21, 27, 33, -6]*/



function multiplica(n,a){
   let novoArr = a.map(e => n*e);
   console.log(novoArr);
}

multiplica(2,[12,10,1,70]);



/*Criar uma função que dado um número n e um array, retorne um novo array com os valores do array anterior * n Ex: (2, [1,3,6,10]) => [2,6,12,20] (3, [7,9,11,-2]) => [21, 27, 33, -6]*/



function string(str,array){
   let novoArray = array.filter((e) => {
      let ok = true
      
      for (i in e){
         if( !(str.includes(e[i])) ) {
            ok = false;
         }
      }
      
      if (ok){
         return e;
      }
   })
   console.log(novoArray);
}

string("abc",["aa","bb","abc","cde","ab"]);

/
    Criar uma função que dado n arrays, retorne um novo array que possua
    apenas os valores que existem em todos os n arrays
    Ex: [1, 2, 3] [3, 3, 7] [9, 111, 3] => [3]
        [120, 120, 110, 2] [110, 2, 130] => [110, 2]



function intersecNum (a,...arrays){
  let arrayIntersec = [];
  for (i in arrays){
    arrayIntersec = arrays[i].filter ( e => { if(a.includes(e)) return e})
        
   }
    
    console.log(arrayIntersec);
  }
  


intersecNum([1, 2, 3], [3, 3, 7], [9, 111, 3]);

/
/
    Crie uma função que dado n arrays, retorne apenas os que tenham a
    soma de seus elementos par
    Ex: [1, 1, 3] [1, 2, 2, 2, 3] [2] => [1, 2, 2, 2, 3] [2]
        [2,2,2,1] [3, 2, 1] => [3,2,1] 
*/



function somaArrays(...arrays){
   let soma = 0;
   
   for (i in arrays){
      currArr = arrays[i]
      for (j in currArr){
         soma +=currArr[j]
      }
     
     if (soma%2==0){
       console.log(currArr);
    }soma = 0;
   
}};

somaArrays([1, 1, 3],[1, 2, 2, 2, 3],[2]);


/*Crie uma função que recebe um array de 2 números e coloque eles em ordem crescente. Não use o método sort.*/


function ordenaArray(a){
  
    let novoArray = [];
    if (a[1]>a[0]){
      novoArray = a;
    }else{
      novoArray.push(a[1]);
      novoArray.push(a[0]);
    }
  
  console.log(novoArray);
  
}

ordenaArray([1,2]);
ordenaArray([2,1]);

